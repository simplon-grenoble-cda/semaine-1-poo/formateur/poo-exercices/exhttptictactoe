Voici la spécification de l'API HTTP à mettre en place pour faire fonctionner notre interface client web du Morpion.

Notez qu'en plus de la spécification fonctionnelle de chaque endpoint, un endpoint peut toujous retourner le status 500 
en cas d'erreur inattendue côté serveur.

Je dois développer un petit frontend web qui permette d'utiliser cette API. En attendant, vous pouvez commencer à
l'implémenter et à la tester en utilisant un logiciel client tel que Postman ou cURL en ligne de commande.

[[_TOC_]]

### New game

#### Description

Crée une nouvelle instance de partie, et retourne l'id de la partie créée. Cet ID servira à référencer la partie
concernée pour toutes les autres requêtes.

Plusieurs parties en parallèle peuvent être jouées.

#### Format requête 

`GET /games/new`

#### Format réponse

Status code : 200 OK

```
{
  "gameId": 1
}
```



### Get game status

#### Description

Obtient un objet JSON qui représente l'état actuel de la partie à l'ID donnée. L'ID de la partie est déduit du chemin de l'URL.

On représente les deux joueurs par la lettre A et B. `"grid"` représente l'état actuel de la grille, avec la liste des cases disposée en tableau
à une dimension (ordre des cases de gauche à droite, puis en suivant le lignes de haut en bas). `"0"` représente une case vide, `"A"` représente
une case jouée par le jouer A, `"B"` représente une case jouée par le joueur B.

Schéma de l'indice des cases dans le tableau : 

```
-------------
| 0 | 1 | 2 |
-------------
| 3 | 4 | 5 |
-------------
| 6 | 7 | 8 |
-------------
```

Si la partie n'est pas terminé, la propriété `"nextRound"` représente le prochain joueur qui doit jouer (`"A"` ou `"B"`). Sinon le champ est `null`.

Si la partie est terminée, la propriété `"winner"` représente le vainqueur. `"A"` ou `"B"`, ou `"draw"` en cas de match null. Sinon le champ est `null`.

#### Format requête

`GET /games/:id/`

#### Format réponse

##### Si la partie à l'ID demandé existe

Status code : 200 OK

```
{
  "gameId": 1,
  "grid": ["0", "A", "0", "B", "0", "0", "0", "0", "0"]
  "nextRound": "A",
  "winner" : null
}
```

##### Si la partie à l'ID demandé n'existe pas

Status code : 404 not found

Body empty

### Play a token on the grid

#### Description

Permet de placer un jeton sur la grille de la partie à l'ID donné. 

La requête doit contenir un corps au format JSON avec les propriétés `"player"` et `"cell"`. `player` représente 
le joueur qui joue (A ou B), et `cell` représente l'indice de la case à jouer (de 0 à 8).

Attention, plusieurs cas de requête invalide sont possibles : 

- si ce n'est pas au tour du joueur qui tente de jouer 
- si la case dans laquelle on tente de jouer n'est pas libre

#### Format de la requête

`POST /games/:id/`

Request body format : 

```
{
  "player": "A",
  "cell": 3
}
```

#### Format de la réponse

##### En cas de succès

status code : 200 OK

Response body : même réponse que pour une requête `GET /games/:id/status`, après que le coup ait été joué.

##### En cas d'erreur : pas au tour du jouer en question

status code : 400 Bad request

Response body : 

```
{
  "error": "bad_player",
  "message" : "Ce n'est pas à vous de jouer"
}
```

##### En cas d'erreur : case non vide

status code : 400 Bad request

Response body : 

```
{
  "error": "cell_not_empty",
  "message" : "La case n'est pas vide"
}
```

# Tester votre serveur

J'ai mis en place une petite suite de tests d'intégration qui permettent de tester si votre serveur rempli les spécifications.

Pour lancer les tests : il vous faut avoir `node` et `npm` installés sur votre machine.

https://nodejs.org/en/download/package-manager/

Tester votre installation : 

```
$ node --version
v12.20.0

$ npm --version
6.14.8
```

Ensuite, rendez vous dans le dossier et utilisez la commande 

```
$ npm run test
```

Les tests vont chercher à faire des requêtes sur un serveur sur l'adresse `http://localhost:3014`. Si vous voulez utiliser
une autre URL (HOST + PORT), vous pouvez configure en créant un fichier `.env` dans le dossier `integration_test` :

```
# fichier .env
TICTACTOE_API_HOST=http://localhost:1337
```

# Frontend 

En cours de développement ! À venir
