const client = require('axios').create({
  baseURL: TICTACTOE_API_HOST
});


it('gets a game ID', async () => {
  let res = await client.get('/games/new');
  let gameId = res.data.gameId;
  expect(typeof gameId).toBe('number');
});

describe('with valid game id', () => {
  let gameId;
  beforeEach(async () => {
    let res = await client.get('/games/new');
    gameId = res.data.gameId;
  });

  it('returns the game status', async () => {
    let res = await client.get(`/games/${gameId}`);
    let status = res.data;
    expect(status.gameId).toEqual(gameId);
    expect(status.grid).toEqual(["0", "0", "0", "0", "0", "0", "0", "0", "0"]);
    expect(status.nextRound).toEqual("A");
    expect(status.winner).toBeNull();
    expect(res.data).toEqual({
      gameId: gameId,
      grid: ["0", "0", "0", "0", "0", "0", "0", "0", "0"],
      nextRound: "A",
      winner: null
    });
  });

  it('post moves', async () => {
    let res = await client.post(`/games/${gameId}`, {
      player: "A",
      cell: 3
    });
    expect(res.data).toEqual({
      gameId: gameId,
      grid: ["0", "0", "0", "A", "0", "0", "0", "0", "0"],
      nextRound: "B",
      winner: null
    });
  });

  it('returns winner', async () => {
    await client.post(`/games/${gameId}`, {
      player: "A",
      cell: 0
    });
    await client.post(`/games/${gameId}`, {
      player: "B",
      cell: 3
    });
    await client.post(`/games/${gameId}`, {
      player: "A",
      cell: 1
    });
    await client.post(`/games/${gameId}`, {
      player: "B",
      cell: 4
    });
    let res = await client.post(`/games/${gameId}`, {
      player: "A",
      cell: 2
    });

    expect(res.data).toEqual({
      gameId: gameId,
      grid: ["A", "A", "A", "B", "B", "0", "0", "0", "0"],
      nextRound: "B",
      winner: "A"
    });
  });

  describe("error cases", () => {
    it("returns a 404 when asking for non existing game id", async () => {
      let randomHighId = Math.floor(Math.random() * 99254740991) + gameId;
      try {
        let res = await client.get(`/games/${randomHighId}`);
      } catch(e) {
        expect(e.response.status).toEqual(404);
      }
    });

    describe('playing move', () => {
      it('bad player attempts to play', async () => {
        await client.post(`/games/${gameId}`, {
          player: "A",
          cell: 3
        });

        try {
          let res = await client.post(`/games/${gameId}`, {
            player: "A",
            cell: 4
          });
        } catch(e) {
          expect(e.response.status).toEqual(400);
          expect(e.response.data).toEqual({
            "error": "bad_player",
            "message" : "Ce n'est pas à vous de jouer"
          });
        }
      });

      it('playing on an empty cell', async () => {
        await client.post(`/games/${gameId}`, {
          player: "A",
          cell: 3
        });

        try {
          let res = await client.post(`/games/${gameId}`, {
            player: "B",
            cell: 3
          });
        } catch(e) {
          expect(e.response.status).toEqual(400);
          expect(e.response.data).toEqual({
            "error": "cell_not_empty",
            "message" : "La case n'est pas vide"
          });
        }
      });
    })
  });
});


